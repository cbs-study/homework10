import re


def student_info(input_string):
    """Функція, яка, використовуючи регулярні вирази, витягіває дані з рядка. який вводиться з клавітури,
    і повертає словник."""

    regex = {
        "Surname": r"Surname: (\w+)",
        "Name": r"Name: (\w+)",
        "Birthdate": r"Birthdate: (\d{2}\.\d{2}\.\d{4})",
        "Email": r"Email address: (\S+@\S+)",
        "Review": r"Review: (.+)",
    }
    extracted_info = {}
    for key, pattern in regex.items():
        match = re.search(pattern, input_string)
        if match:
            extracted_info[key] = match.group(1)

    return extracted_info


# Рядок, в якому є інформація про прізвище, ім'я, дату народження, електронну адресу та відгук про курси учня.
input_string = input(
    "Enter a student information: "
)  # Surname: Smith, Name: John, Birthdate: 01.04.2005, Email: j.smith@gmail.com, Review: Highly recommend it!

student = student_info(input_string)
print(student)
