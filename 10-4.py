def analyze_text(text):
    """Функція, яка аналізуватє текст, що надходить до неї, і виводе тільки унікальні слова на екран,
    загальну кількість слів і кількість унікальних слів."""
    words = text.split()
    unique_words = set(words)
    print("Unique words: ")
    for word in unique_words:
        print(word)

    print(f"Words quantity: {len(words)}")
    print(f"Unique words quantity: {len(unique_words)}")


text = (
    "The sky blushes with shades of pink and orange as the sun goes below the horizon, "
    "painting a masterpiece of brief beauty."
)
analyze_text(text)
