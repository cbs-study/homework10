def last_3_characters(proposal):
    """Функція, яка друкує на екран останні 3 символи кожного слова пропозиції, яку вводить користувач з клавіатури."""

    words = proposal.split()
    for word in words:
        print(word[-3:])


proposal = input("Enter proposal: ")
last_3_characters(proposal)
