import re

""" Функція, яка за допомогою регулярних виразів з файлу витягує дані про дату народження,
телефон та електронну адресу. Дані потрібно записати до іншого файлу."""


def private_info(input_file, output_file):
    # Відкриття вхідного файлу 'input' для читання та 'output' файлу для запису
    with open(input_file) as f_in, open(output_file, "w") as f_out:
        content = f_in.read()
        # Пошук дати народження, телефону і електронної адреси
        birth_dates = re.findall(r"\b\d{2}-\d{2}-\d{4}\b", content)
        phones = re.findall(r"\b\d{3}-\d{3}-\d{4}\b", content)
        emails = re.findall(
            r"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b", content
        )

        f_out.write("Birth dates:\n")
        for bd in birth_dates:
            f_out.write(f"{bd}\n")

        f_out.write("\nPhones:\n")
        for phone in phones:
            f_out.write(f"{phone}\n")

        f_out.write("\nEmails:\n")
        for email in emails:
            f_out.write(f"{email}\n")


# Пошук дати народження у форматі дд-мм-гггг, телефонні номери у форматі 111-111-1111 та
# електронні адреси з файлу 'input'
private_info("input.txt", "output.txt")
