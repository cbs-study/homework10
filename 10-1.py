import re
from collections import Counter

"""Функція, яка за допомогою регулярних виразів розбиває текст на окремі слова і знаходить частоту окремих слів."""


def word_frequency(text):
    pattern = r"\b\w+"
    words = re.findall(pattern, text)
    word_freq = Counter(words)
    return word_freq


text = (
    "The sky blushes with shades of pink and orange as the sun goes below the horizon, "
    "painting a masterpiece of brief beauty."
)

result = word_frequency(text.lower())
print(result)
